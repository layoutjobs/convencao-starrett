//Função para Compilar o Sass.
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

//Função para compilar o SASS
function compilarSass() {
    return gulp
        .src('styles/mystyles.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(autoprefixer({
            Browserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
}

//Função para executar o SASS
gulp.task('sass', compilarSass);


//Função para Concat do JS
function gulpJS() {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'scripts/easing.min.js',
        'scripts/hoverIntent.js',
        'scripts/jquery.sticky.js',
        'scripts/superfish.js',
        'scripts/superfish.min.js',
        'scripts/ekko-lightbox.js',
        'scripts/base.js'
        
    ])


        .pipe(concat('main.js'))
        .pipe(uglify('main.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(browserSync.stream());
}

gulp.task('mainjs', gulpJS);


//Função para Iniciar o Browser Sync
function browser() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
}

//Tarefa para iniciar o Browser Sync
gulp.task('browser-sync', browser);


//Função para "Espionar as Alterações no Projeto"
function watchproject() {
    gulp.watch('styles/theme/*.scss', compilarSass);
    gulp.watch('scripts/*.js', gulpJS);
    gulp.watch('*.html').on('change', browserSync.reload);
}

//Tarefa para iniciar o Watch
gulp.task('watch', watchproject);

//Tarefa padrão para executar o Gulp 
gulp.task('default', gulp.parallel('watch', 'browser-sync', 'sass', 'mainjs'));