<?php

//ESCOPO
$host_smtp = '';
$host_port = '';
$email_envio = ''; //Inserir Login do E-mail
$email_pass = ''; //Inserir Senha

//Váriveis de Formulário
$name = !empty($_POST['nome']) ? filter_var($_POST['nome'], FILTER_SANITIZE_STRING) : null;
$email = !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : null;
$mensagem = !empty($_POST['mensagem']) ? filter_var($_POST['mensagem'], FILTER_SANITIZE_STRING) : null;
$anexo = $_FILES["arquivo"];

date_default_timezone_set('Brazil/East');

$date = date('d/m/Y');
$hour = date('H:i');

# Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
require_once("PHPMailer/class.phpmailer.php");
require_once("PHPMailer/class.smtp.php");

# Inicia a classe PHPMailer
$mail = new PHPMailer();

# Define os dados do servidor e tipo de conexão
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->Host = $host_smtp; # Endereço do servidor SMTP
$mail->Port = $host_port; // Porta TCP para a conexão
$mail->SMTPAutoTLS = false; // Utiliza TLS Automaticamente se disponível
$mail->SMTPAuth = true; # Usar autenticação SMTP - Sim
$mail->Username = $email_envio; # Usuário de e-mail
$mail->Password = $email_pass; // # Senha do usuário de e-mail
$mail->SMTPKeepAlive = true;
$mail->Timeout = 9000;

# Define o remetente (você)
$mail->From = "igor@layoutnet.com.br"; # Seu e-mail
$mail->FromName = "Convenção Mailing"; // Seu nome

# Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();


# Define os destinatário(s)
//$mail->AddAddress("nprandini@starrett.com.br");
//$mail->AddAddress("carloshpht@gmail.com");
$mail->AddAddress("igor@layoutnet.com.br");


# Define os dados técnicos da Mensagem
$mail->IsHTML(true); # Define que o e-mail será enviado como HTML
$mail->CharSet = 'utf-8'; # Charset da mensagem (opcional)
//$mail->SMTPDebug = 0; #Ativar Caso queira debugar o Envio de e-mail
$mail->SMTPSecure = "ssl";


# Define a mensagem (Texto e Assunto)
$mail->Subject = "Convenção Internacional de Vendas Starrett"; # Assunto da mensagem
$mail->AltBody = "Este é o corpo da mensagem de teste, somente Texto! \r\n :)";

//Anexo
$mail->AddAttachment($anexo['tmp_name'], $anexo['name']);

$msg = '<table width="650" cellspacing="0" cellpadding="0" border="0" bordercolor="#fff" align="center">
			<tr style="background:#f6d330;">
				<td align="center"> <br> <br> <img style="width: 50%;" src="http://drive.layoutnet.com.br/usuarios/convencao-starrett/assets/images/logo.png" alt="logo"> <br> <br> <br></td>
			</tr>
			<tr style="background: #bb262c">
				<td>
				<br>
				<br>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px"> ' . $name . ', preencheu o formulário no site da Convenção Internacional de Vendas Starrett</p></b>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px">E-mail: ' . $email . '</p></b>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px">Mensagem: ' . $mensagem . '</p></b>
				<b><p style="color: #fff; text-align: left; font-family: Roboto Condensed, sans-serif; margin-left: 50px">Enviado em: ' . $date . ' , ' . $hour . '</p></b>
				<br>
				<br>
				</td>
			</tr>
		</table>';

$mail->Body = $msg;

# Envia o e-mail
$enviado = $mail->Send();


if ($enviado) {
	echo "<script>alert('Sua Mensagem Foi Enviada com Sucesso');</script>";
	echo "<meta HTTP-EQUIV='refresh' CONTENT='1;URL=../'>";
	exit;
} else {
	echo "<script>alert('Sua mensagem não pode ser enviada, tente entrar em contato pelo telefone: (55-11) 2118-8311');</script>";
	echo "<meta HTTP-EQUIV='refresh' CONTENT='1;URL=../'>";
}
